import axios from 'axios';
import {config} from '../config';

export function getWeather(city) {
    return axios.get(`${config.url_api_back}?q=${city}&APPID=${config.APIKEY}&units=metric&&lang=fr`)
        .then((response)=>{
            return response.data
        })
        .catch(function (error) {
		    console.log(error);
		  });
}
