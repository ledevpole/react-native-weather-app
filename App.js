import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Image} from 'react-native';
import {getWeather} from './api/meteo_api';

export default class App extends React.Component {

  constructor(props){
    super(props)
		this.state = {
      target:"Paris",
			data: {},
      temperature:"",
      meteo:"",
      meteo_icon:"01n"
		}
	}

    onPress = () => {
      getWeather(this.state.target)
      .then((res) => {
        console.log(this.state.target)
        this.setState({data : res})
        console.log(this.state.data)
        this.setState({temperature : `${res.main.temp} °C`})
        this.setState({meteo : `${res.weather[0].description}`})
        this.setState({meteo_icon : `http://openweathermap.org/img/wn/${res.weather[0].icon}@2x.png`})
        console.log(this.state.temperature)
      })
    }

    render(){
      return (
        <View style={styles.container}>
          <Text>Tapez le nom d'une ville pour obtenir la météo</Text>
          <TextInput style="backgroundColor:grey"
          ref= {(el) => { this.target = el; }}
          onChangeText={(target) => this.setState({target: target})}
          value={this.state.target}>
            </TextInput>
            <Text>{this.state.temperature}</Text>
            <Text>{this.state.meteo}</Text>
            <Image
          style={{width: 50, height: 50}}
          source={{uri: this.state.meteo_icon }}></Image>
          <TouchableOpacity style={styles.boutton} onPress={this.onPress}>
            <Text>Get météo</Text>
          </TouchableOpacity>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  boutton: {
    backgroundColor:"green",
    padding: 10
  }
});
